import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-all-users',
  templateUrl: './all-users.component.html',
  styleUrls: ['./all-users.component.scss']
})
export class AllUsersComponent implements OnInit {
  users = [{"id":"2","username":"test-user","firstName":"nextName","gender":"1","createdDate":"2019-08-20 08:07:05","updatedDate":"2019-08-20 08:09:53"},{"id":"3","username":"","firstName":"nextName","gender":"1","createdDate":"2019-08-20 08:07:16","updatedDate":"0000-00-00 00:00:00"}];

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.http.get('http://localhost/api/user');
  }
}
