<?php
require 'user/UserController.php';

class Router
{
    private $url = '';
    private $requestMethod = 'GET';

    public function getUrl() {
        return $this->url;
    }

    public function setUrl($url) {
        $this->url = $url;
    }

    public function getRequestMethod() {
        return $this->requestMethod;
    }

    public function setRequestMethod($requestMethod) {
        $this->requestMethod = $requestMethod;
    }

    public function go() {
        $urlParts = explode('/', $this->getUrl());
        $isUserController = isset($urlParts[2]) && $urlParts[2] == 'user';
        $userId = isset($urlParts[3]) && is_numeric($urlParts[3]) ? (int)$urlParts[3] : null;

        if (!$isUserController) {
            return null;
        }

        if ($userId && $this->requestMethod == 'GET') {
            return UserController::get($userId);
        }

        if ($this->requestMethod == 'GET') {
            return UserController::getAll();
        }

        if ($this->requestMethod == 'POST' && $userId) {
            return UserController::update($userId);
        }

        if ($this->requestMethod == 'POST' && !$userId) {
            return UserController::create();
        }

        if ($this->requestMethod == 'DELETE' && $userId) {
            return UserController::delete($userId);
        }

        return null;
    }
}