<?php

class Database
{
    /** @var mysqli $connection */
    private $connection = null;

    public function init() {
        $this->connection = new mysqli('127.0.0.1', 'root', '', 'test_task');

        if ($this->connection->connect_errno) {
            echo $this->connection->connect_errno . "\n";
            echo $this->connection->connect_error . "\n";

            exit;
        }

        $this->connection->query("
            CREATE TABLE IF NOT EXISTS `test_task`.`user` ( 
                `id` INT NOT NULL AUTO_INCREMENT , 
                `username` VARCHAR(255) NOT NULL , 
                `firstName` VARCHAR(255) NOT NULL , 
                `gender` TINYINT(1) NOT NULL , 
                `createdDate` DATETIME NOT NULL , 
                `updatedDate` DATETIME NOT NULL , 
                PRIMARY KEY (`id`)
            ) ENGINE = InnoDB;");
    }

    /**
     * @param string $sql
     * @return bool|mysqli_result
     */
    public function query($sql) {
        if ($this->connection !== null) {
            return $this->connection->query($sql);
        }

        return false;
    }
}