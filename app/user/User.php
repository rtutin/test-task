<?php

require $_SERVER['DOCUMENT_ROOT'] . '/app/Database.php';

class User
{
    /** @var int $id */
    private $id = null;
    /** @var string $username */
    private $username = null;
    /** @var string $firstName */
    private $firstName = null;
    /** @var bool $gender */
    private $gender = 'false';
    /** @var DateTime $createdDate */
    private $createdDate = null;
    /** @var DateTime $updatedDate */
    private $updatedDate = null;

    static function get($id) {
        $db = new Database();
        $db->init();
        $obj = $db->query("SELECT * FROM `user` WHERE `id` = " . $id)->fetch_object();

        if ($obj !== null) {
            $user = new User();

            $user->setId($obj->id);
            $user->setUsername($obj->username);
            $user->setFirstName($obj->firstName);
            $user->setGender($obj->gender);
            $user->setCreatedDate($obj->createdDate);
            $user->setUpdatedDate($obj->updatedDate);

            return $user;
        } else {
            return null;
        }
    }

    public function save() {
        $sql = '';
        $db = new Database();
        $db->init();

        if ($this->id === null) {
            $sql = "INSERT INTO `user`(`username`, `firstName`, `gender`, `createdDate`) VALUES (
                '" . $this->username . "', '" . $this->firstName . "', " . $this->gender . ", NOW())";
        }

        if ($this->id) {
            $sql = "UPDATE `user` SET `firstName` = '" . $this->firstName . "',
            `gender` = " . $this->gender . ", `updatedDate` = NOW() WHERE `id` = " . $this->id;
        }

        return $db->query($sql);
    }

    static function delete($id) {
        $db = new Database();
        $db->init();

        return $db->query("DELETE FROM `user` WHERE `id` = " . $id);
    }

    static function getAll() {
        $db = new Database();
        $db->init();

        return $db->query("SELECT * FROM `user` WHERE 1")->fetch_all(MYSQLI_ASSOC);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username): void
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return bool
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param bool $gender
     */
    public function setGender($gender): void
    {
        $this->gender = $gender;
    }

    /**
     * @return DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * @param DateTime $createdDate
     */
    public function setCreatedDate($createdDate): void
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedDate()
    {
        return $this->updatedDate;
    }

    /**
     * @param DateTime $updatedDate
     */
    public function setUpdatedDate($updatedDate): void
    {
        $this->updatedDate = $updatedDate;
    }
}