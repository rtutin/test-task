<?php

require 'User.php';

class UserController
{
    static function get($id) {
        return [];
    }

    static function create() {
        $request = $_POST;

        if (!empty($request['username'])) {
            $user = new User();
            $user->setUsername($request['username']);

            if (!empty($request['firstName'])) {
                $user->setFirstName($request['firstName']);
            }

            if (!empty($request['gender'])) {
                $user->setFirstName($request['gender']);
            }

            $user->save();
        }
    }

    static function update($id) {
        $request = $_POST;

        $user = User::get($id);

        if ($user === null) {
            return false;
        }

        if (!empty($request['firstName'])) {
            $user->setFirstName($request['firstName']);
        }

        if (!empty($request['gender'])) {
            $user->setGender($request['gender']);
        }

        return $user->save();
    }

    static function delete($id) {
        return User::delete($id);
    }

    static function getAll() {
        return User::getAll();
    }
}